package org.example.dghoughihafsa.Repository;

import org.example.dghoughihafsa.Entity.Associe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssocieRepository extends JpaRepository<Associe,Long> {
}
