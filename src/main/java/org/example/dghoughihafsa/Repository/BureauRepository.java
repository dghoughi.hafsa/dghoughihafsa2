package org.example.dghoughihafsa.Repository;

import org.example.dghoughihafsa.Entity.Bureau;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BureauRepository extends JpaRepository<Bureau,Long> {
}
