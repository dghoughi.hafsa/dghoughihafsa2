package org.example.dghoughihafsa.Repository;

import org.example.dghoughihafsa.Entity.PleinTemps;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PleinTempsRepository extends JpaRepository<PleinTemps,Long> {
}
