package org.example.dghoughihafsa.Repository;

import org.example.dghoughihafsa.Entity.Prof;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProfRepository extends JpaRepository<Prof,Long> {
}
