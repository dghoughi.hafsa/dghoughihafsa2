package org.example.dghoughihafsa.Entity;

import jakarta.persistence.*;

import java.util.List;

@Entity
public class Universite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUniversite;
    private String nomUniversite;

    public Universite() {
    }

    @OneToOne
    @JoinColumn(name = "id")
    private Person person;

    @OneToMany(mappedBy = "universite")
    private List<Bureau> bureau;


    public Long getIdUniversite() {
        return idUniversite;
    }

    public void setIdUniversite(Long idUniversite) {
        this.idUniversite = idUniversite;
    }

    public String getNomUniversite() {
        return nomUniversite;
    }

    public void setNomUniversite(String nomUniversite) {
        this.nomUniversite = nomUniversite;
    }

    public Universite(String nomUniversite) {
        this.nomUniversite = nomUniversite;
    }

    @Override
    public String toString() {
        return "Universite{" +
                "idUniversite=" + idUniversite +
                ", nomUniversite='" + nomUniversite + '\'' +
                '}';
    }
}
