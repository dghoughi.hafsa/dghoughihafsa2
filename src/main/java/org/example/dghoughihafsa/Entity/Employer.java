package org.example.dghoughihafsa.Entity;

import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToOne;

public class Employer extends Person {
    public Employer() {
    }

    public Employer(String nom) {
        super(nom);
    }

    @ManyToOne
    @JoinColumn(
            name = "idEmp",
            referencedColumnName = "employes"
    )
    private Bureau bureau;

}
