package org.example.dghoughihafsa.Entity;

import jakarta.persistence.*;

@Entity
public class Salle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idSalle;
    private Long nbSiege;

    @ManyToOne
    @JoinColumn(
            name = "universite"
    )
    private Universite universite;

}
