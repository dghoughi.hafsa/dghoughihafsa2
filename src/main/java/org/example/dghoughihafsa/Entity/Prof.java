package org.example.dghoughihafsa.Entity;

import jakarta.persistence.*;

@Inheritance(strategy = InheritanceType.JOINED)
public class Prof extends Person {

    @ManyToOne
    @JoinColumn(
            name = "idDept"
    )
    private Departement departement;

    public Prof() {
    }

    public Prof(String nom) {
        super(nom);
    }
}
