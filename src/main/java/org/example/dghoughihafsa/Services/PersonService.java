package org.example.dghoughihafsa.Services;

import org.example.dghoughihafsa.Entity.Person;
import org.example.dghoughihafsa.Repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {
    private PersonRepository personRepository;
    @Autowired

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person add(Person person) {
        return personRepository.save(person);
    }

    public Person modify(Person person) {
        return person;
    }
    public void afficher(Person person){
        Long idP;
        idP = person.getId();
        System.out.println("person = " + person);
    }
}
