package org.example.dghoughihafsa.Controller;

import org.example.dghoughihafsa.Entity.Person;
import org.example.dghoughihafsa.Services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/person")
public class PersonController {
    private PersonService personService;
    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping(value = "/NouvellePerson")
    public Person creer(Person person){
        return personService.add(person);
    }

    @PostMapping
    public Person modifier(Person person){
        return personService.modify(person);

    }

    @GetMapping(value = "{id}")
    public void afficher(Person person){
        personService.afficher(person);
    }



}
